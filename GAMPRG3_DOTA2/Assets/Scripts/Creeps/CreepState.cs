﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/CreepState")]
public class CreepState : ScriptableObject
{
    public CreepActions[] actions;
    public CreepTransition[] transitions;
    public Color sceneGizmoColor = Color.grey;

    public void UpdateState(CreepStateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    private void DoActions(CreepStateController controller)
    {
        for(int i = 0;i<actions.Length;i++)
        {
            actions[i].Act(controller);
        }
    }

    private void CheckTransitions(CreepStateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.CreepDecide(controller);

            if(decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }
}
