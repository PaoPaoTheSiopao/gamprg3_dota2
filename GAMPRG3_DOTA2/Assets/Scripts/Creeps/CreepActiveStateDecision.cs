﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/CreepActiveState")]
public class CreepActiveStateDecision : CreepDecision
{
    public override bool CreepDecide(CreepStateController controller)
    {
        //bool chaseTargetIsActive = controller.chaseTarget.gameObject.activeSelf;
        //return chaseTargetIsActive;
        return false;
    }
}
