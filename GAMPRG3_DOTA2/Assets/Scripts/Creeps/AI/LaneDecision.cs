﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/LaneDecision")]
public class LaneDecision : CreepDecision
{
    public override bool CreepDecide(CreepStateController controller)
    {
        Debug.Log("LaneDec");
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    // Update is called once per frame
    private bool Look(CreepStateController controller)
    {
        if(controller.creep.ScanForEnemies())
        {
            controller.creep.unitAgent.ResetPath();
            return true;
        }
        else
        {
            return false;
        }
    }
}
