﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/AttackTargetAction")]
public class AttackTargetAction : CreepActions
{
    public override void Act(CreepStateController controller)
    {
        Attack(controller);
    }

    private void Attack(CreepStateController controller)
    {
        //Debug.Log("Unit Agent Status : "+controller.creep.unitAgent.isStopped);
        //Debug.Log("Unit Agent Distance : " + controller.creep.unitAgent.remainingDistance);
        controller.creep.FollowTargetUnit();
        controller.creep.AttackTarget();
    }

}