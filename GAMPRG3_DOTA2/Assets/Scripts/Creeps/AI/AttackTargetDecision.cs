﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/AttackTargetDecision")]
public class AttackTargetDecision : CreepDecision
{
    public override bool CreepDecide(CreepStateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    // Update is called once per frame
    private bool Look(CreepStateController controller)
    {
        Debug.Log(controller.creep.target);
        if (controller.creep.target != null)
        {
            Debug.Log("Dec False");
            return false;
        }
        else
        {
            Debug.Log("Dec True");
            return true;
        }
    }
}
