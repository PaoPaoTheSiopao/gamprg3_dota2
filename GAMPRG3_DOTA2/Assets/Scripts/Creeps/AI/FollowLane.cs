﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/FollowLaneAction")]
public class FollowLane : CreepActions
{
    public override void Act(CreepStateController controller)
    {
        Debug.Log("Follow Lnae");
        Patrol(controller);
    }

    private void Patrol(CreepStateController controller)
    {
        if(controller.creep.unitAgent.remainingDistance<0.5f)
        {
            controller.creep.NextWaypoint();
        }
    }

}
