﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CreepActions : ScriptableObject
{
    public abstract void Act(CreepStateController controller);
}
