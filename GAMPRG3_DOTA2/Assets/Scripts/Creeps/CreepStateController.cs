﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CreepStateController : MonoBehaviour
{
    [HideInInspector] public FieldOfVision fov;
    [HideInInspector] public Creeps creep;

    public CreepState currentState;
    public CreepState remainState;

    private float stateTimeElapsed;

    void Start()
    {
        fov = GetComponent<FieldOfVision>();
        creep = GetComponent<Creeps>();
    }
    void Update()
    {
        if (currentState != null)
        {
            currentState.UpdateState(this);
        }
    }

    public void TransitionToState(CreepState nextState)
    {
        if(nextState!=remainState)
        {
            currentState = nextState;
            OnStateExit();
        }
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;

        return (stateTimeElapsed >= duration);
    }

    private void OnStateExit()
    {
        stateTimeElapsed = 0;
    }
    
   
}
