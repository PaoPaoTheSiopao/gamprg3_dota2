﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CreepDecision : ScriptableObject
{
    public abstract bool CreepDecide(CreepStateController controller);
}
