﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CreepTransition
{
    public CreepDecision decision;
    public CreepState trueState;
    public CreepState falseState;
}
