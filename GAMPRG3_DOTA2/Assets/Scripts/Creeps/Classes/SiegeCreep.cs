﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiegeCreep : Creeps
{
    public GameObject rangeProj;

    public Transform spwPnt;
    public override void DamageTarget()
    {
        GameObject proj = Instantiate(rangeProj, spwPnt.position, transform.rotation);
        proj.GetComponent<TowerProjectile>().target = target.transform;
        proj.GetComponent<TowerProjectile>().origin = this.gameObject;
    }
    public override void CreepSetup()
    {
        switch (creepTyp)
        {
            case CreepTypes.Normal:
                GetComponent<HealthScript>().CreepInitialHealth(875);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 3f;
                attackRange = 43.13f;
                GetComponent<ArmorScript>().baseArmor = 0;
                GetComponent<LvlExpScript>().creepExp = 88;
                GetComponent<BountyScript>().creepBounty = new Vector2(59, 72);
                damage = Random.Range(35, 46);
                break;
            case CreepTypes.Super:
                GetComponent<HealthScript>().CreepInitialHealth(875);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 3f;
                attackRange = 43.13f;
                GetComponent<ArmorScript>().baseArmor = 0;
                GetComponent<LvlExpScript>().creepExp = 88;
                GetComponent<BountyScript>().creepBounty = new Vector2(59, 72);
                damage = Random.Range(51, 62);
                break;
        }
    }
}
