﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCreep : Creeps
{
    public override void CreepSetup()
    {
        //Debug.Log("CreepSetup : " + creepTyp);
        switch(creepTyp)
        {
            case CreepTypes.Normal:
                GetComponent<HealthScript>().CreepInitialHealth(550);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 6.25f;
                GetComponent<ArmorScript>().baseArmor = 2;
                GetComponent<LvlExpScript>().creepExp = 57;
                GetComponent<BountyScript>().SetCreepBounty(new Vector2(29, 34));
                damage = Random.Range(19, 23);
                break;
            case CreepTypes.Super:
                GetComponent<HealthScript>().CreepInitialHealth(700);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 6.25f;
                GetComponent<ArmorScript>().baseArmor = 3;
                GetComponent<LvlExpScript>().creepExp = 25;
                GetComponent<BountyScript>().creepBounty = new Vector2(17, 23);
                damage = Random.Range(36, 44);
                break;
            case CreepTypes.Mega:
                GetComponent<HealthScript>().CreepInitialHealth(1270);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 6.25f;
                GetComponent<ArmorScript>().baseArmor = 3;
                GetComponent<LvlExpScript>().creepExp = 25;
                GetComponent<BountyScript>().creepBounty = new Vector2(17, 23);
                damage = Random.Range(96, 104);
                break;
        }
    }

    public override void CreepScale()
    {
        float scaleMult = GameManager.Instance.GetGameTime() / 450;
        GetComponent<HealthScript>().UpdateCreepHealth(scaleMult);
        damage = damage + (scaleMult * 1);
        GetComponent<BountyScript>().baseGoldBounty += (1 * scaleMult);
    }
}
