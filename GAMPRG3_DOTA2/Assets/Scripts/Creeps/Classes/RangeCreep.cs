﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCreep : Creeps
{
    public GameObject rangeProj;

    public Transform spwPnt;
    public override void DamageTarget()
    {
        GameObject proj = Instantiate(rangeProj,spwPnt.position,transform.rotation);
        proj.GetComponent<TowerProjectile>().target = target.transform;
        proj.GetComponent<TowerProjectile>().origin = this.gameObject;
        proj.GetComponent<TowerProjectile>().damage = damage;
    }

    public override void CreepSetup()
    {
        switch (creepTyp)
        {
            case CreepTypes.Normal:
                GetComponent<HealthScript>().CreepInitialHealth(300);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 31.25f;
                GetComponent<ArmorScript>().baseArmor = 0;
                GetComponent<LvlExpScript>().creepExp = 69;
                GetComponent<BountyScript>().creepBounty = new Vector2(43, 52);
                damage = Random.Range(21, 26);
                break;
            case CreepTypes.Super:
                GetComponent<HealthScript>().CreepInitialHealth(700);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 31.25f;
                GetComponent<ArmorScript>().baseArmor = 1;
                GetComponent<LvlExpScript>().creepExp = 25;
                GetComponent<BountyScript>().creepBounty = new Vector2(19, 21);
                damage = Random.Range(41, 46);
                break;
            case CreepTypes.Mega:
                GetComponent<HealthScript>().CreepInitialHealth(1015);
                unitAgent.speed = unitMovementSpeed;
                unitAgent.acceleration = unitMovementSpeed;
                baseAttackTime = 1f;
                attackRange = 31.25f;
                GetComponent<ArmorScript>().baseArmor = 1;
                GetComponent<LvlExpScript>().creepExp = 22;
                GetComponent<BountyScript>().creepBounty = new Vector2(19, 25);
                damage = Random.Range(131, 136);
                break;
        }
    }

    public override void CreepScale()
    {
        float scaleMult = GameManager.Instance.GetGameTime() / 450;
        GetComponent<HealthScript>().UpdateCreepHealth(scaleMult);
        damage = damage + (scaleMult * 1);
        GetComponent<BountyScript>().baseGoldBounty += (1 * scaleMult);
    }
}
