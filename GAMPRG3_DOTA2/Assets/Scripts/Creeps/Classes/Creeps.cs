﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creeps : UnitScript
{
    enum CreepState { LaneState,AttackState};
    public int pathCount = 1;


    [SerializeField]CreepState curState = CreepState.LaneState;
    public CreepTypes creepTyp;

    public List<Material> color;
    private void Start()
    {
        unitAgent = GetComponent<NavMeshAgent>(); 
        unitAgent.updateRotation = false;
        agentPath = new NavMeshPath();
        fov = GetComponent<FieldOfVision>();
        faction = GetComponent<Factions>();
        switch (faction.unitFaction)
        {
            case Faction.Dire:
                pathIndex = unitPath.Count - 1;
                GetComponent<Renderer>().sharedMaterial = color[1];
                break;
            case Faction.Radiant:
                pathIndex = 0;
                GetComponent<Renderer>().sharedMaterial = color[0];
                break;
        }
        unitAgent.SetDestination(unitPath[pathIndex].position);
        CreepSetup();
        if (GameManager.Instance.GetGameTime()/450>0)
        {
            Debug.Log("CreepScale");
            CreepScale();
        }
    }
    private void Update()
    {
        //Debug.Log("Faction : " + faction.unitFaction);
        //Debug.Log("Destination : " + unitPath[pathIndex].name);
        //Debug.Log("Dest : "+unitAgent.destination+"Remain : " + unitAgent.remainingDistance);
        //Debug.Log("Status : " + unitAgent.pathStatus);
        if (unitAgent.hasPath)
        {
            transform.rotation = Quaternion.LookRotation(ReturnAngleBetween(unitAgent.path.corners[1]));
        }
        DetermineState();
        switch (curState)
        {
            case CreepState.LaneState:
                if (unitAgent.remainingDistance < 0.5f&&target==null)
                {
                    unitAgent.velocity = Vector3.zero;
                    NextWaypoint();
                    unitAgent.speed = unitMovementSpeed;
                    unitAgent.acceleration = unitMovementSpeed;
                }
                break;
            case CreepState.AttackState:
                //NavMesh.CalculatePath(this.transform.position, unitPath[ReturnNearestWayPoint()].position, NavMesh.AllAreas, agentPath);
                FollowTargetUnit();
                AttackTarget();
                break;
        }
    }

    public void DetermineState()
    {
        switch (curState)
        {
            case CreepState.LaneState:
                if(ScanForEnemies())
                {
                    curState = CreepState.AttackState;
                }
                break;
            case CreepState.AttackState:
                {
                    if (target==null)
                    {
                        if(ScanForEnemies())
                        {

                        }
                        else
                        {
                            unitAgent.ResetPath();
                            FindNearestWayPoint();
                            curState = CreepState.LaneState;
                        }
                    }
                    else if(unitAgent.remainingDistance>chaseRange)
                    {
                        target = null;
                        unitAgent.ResetPath();
                        unitAgent.SetDestination(unitPath[pathIndex].position);
                        //FindNearestWayPoint();
                        curState = CreepState.LaneState;
                    }
                }
                break;
        }
    }
    public void SetParameters(List<Transform> path, Factions uFact)
    {
        unitPath = path;
        GetComponent<Factions>().unitFaction = uFact.unitFaction;
    }

    public virtual void CreepSetup()
    {
        Debug.Log("CreepSetup Creeps ");
        unitAgent.speed = unitMovementSpeed;
        unitAgent.acceleration = unitMovementSpeed;
        baseAttackTime = 1f;
        attackRange = 6.25f;
        GetComponent<LvlExpScript>().creepExp = 57;
        damage = Random.Range(19,23);
    }
    public virtual void CreepScale()
    {
        
    }

    public void CreepDeath()
    {

    }
}
