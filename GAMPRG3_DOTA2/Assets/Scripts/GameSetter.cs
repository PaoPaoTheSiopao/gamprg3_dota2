﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetter : MonoBehaviour
{
    public Light worldLight;
    public GameObject uiCanvas;

    public Transform direSpawn;
    public Transform radiantSpawn;

    public GameObject radTopHud;
    public GameObject dirTopHud;
    void Start()
    {
        //Debug.Log("GameSetter Awoken");
        GameState.Instance.worldLight = worldLight;
        GameState.Instance.uiCanvas = uiCanvas;

        GameManager.Instance.radiantSpawn = radiantSpawn;
        GameManager.Instance.direSpawn = direSpawn;

        GameManager.Instance.direTopHud = dirTopHud;
        GameManager.Instance.radiantTopHud = radTopHud;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
