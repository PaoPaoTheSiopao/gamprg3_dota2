﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : Singleton<GameState>
{
    public Light worldLight;
    public bool isDay = false;
    public int counter =1;


    public GameObject uiCanvas;
    private void Start()
    {
        isDay = false;
    }
    void Update()
    {
        if (GameManager.Instance.GetGameTime() % 600 == 0)
        {
            if (counter == 1)
            {
                if (isDay)
                {
                    isDay = false;
                    worldLight.intensity = 0.5f;
                }
                else
                {
                    isDay = true;
                    worldLight.intensity = 1f;
                }
                counter = 0;
            }
        }
        else
        {
            counter = 1;
        }
    }

    public void AssignWinner(Faction fact)
    {
        switch (fact)
        {
            case Faction.Dire:
                Debug.Log("Radiant Won");
                uiCanvas.GetComponent<UICanvasScript>().stateBanner.SetActive(true);
                uiCanvas.GetComponent<UICanvasScript>().stateBanner.GetComponent<Text>().text = "Radiant Won";
                Time.timeScale = 0;
                break;
            case Faction.Radiant:
                Debug.Log("Dire Won");
                uiCanvas.GetComponent<UICanvasScript>().stateBanner.SetActive(true);
                uiCanvas.GetComponent<UICanvasScript>().stateBanner.GetComponent<Text>().text = "Dire Won";
                Time.timeScale = 0;
                break;
        }
    }

    public void UpdateUICanvas()
    {
        UIAttributes ui = uiCanvas.GetComponent<UIAttributes>();
        ui.UpdatePlayer();
    }
}
