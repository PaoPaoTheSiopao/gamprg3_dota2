﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StateController : MonoBehaviour
{
    [HideInInspector] public HeroUnit unit;

    public State currentState;
    public State remainState;

    private float stateTimeElapsed;

    void Start()
    {
        unit = GetComponent<HeroUnit>();
    }
    void Update()
    {
        if (currentState != null)
        {
            currentState.UpdateState(this);
        }
    }

    public void TransitionToState(State nextState)
    {
        if(nextState!=remainState)
        {
            currentState = nextState;
            OnStateExit();
        }
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;

        return (stateTimeElapsed >= duration);
    }

    private void OnStateExit()
    {
        stateTimeElapsed = 0;
    }
    
   
}
