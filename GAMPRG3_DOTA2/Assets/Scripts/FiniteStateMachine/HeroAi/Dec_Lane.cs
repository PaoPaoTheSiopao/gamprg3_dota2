﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Dec_Lane")]
public class Dec_Lane : Decision
{
    public override bool Decide(StateController controller)
    {
        return Decision(controller);
    }

    private bool Decision(StateController controller)
    {
        return controller.unit.ScanForEnemies();
    }
}
