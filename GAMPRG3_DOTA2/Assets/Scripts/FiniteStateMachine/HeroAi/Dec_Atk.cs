﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Dec_Atk")]
public class Dec_Atk : Decision
{
    public override bool Decide(StateController controller)
    {
        return Decision(controller);
    }

    private bool Decision(StateController controller)
    {
        if(controller.unit.target !=null)
        {
            if(controller.unit.unitAgent.remainingDistance>controller.unit.chaseRange)
            {
                controller.unit.target = null;
                controller.unit.FindNearestWayPoint();
                return true;
            }
            else
            {
                return false;
            }
        }
        else 
        {
            if(controller.unit.ScanForEnemies())
            {
                return false;
            }
            else
            {
                controller.unit.FindNearestWayPoint();
                return true;
            }
        }
    }
}
