﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Act_Lane")]
public class Act_Lane : Actions
{
    public override void Act(StateController controller)
    {
        Action(controller);
    }

    private void Action(StateController controller)
    {
        if (controller.unit.unitAgent.remainingDistance < 0.5f && controller.unit.target == null)
        {
            controller.unit.unitAgent.velocity = Vector3.zero;
            controller.unit.NextWaypoint();
            controller.unit.unitAgent.speed = controller.unit.unitMovementSpeed;
            controller.unit.unitAgent.acceleration = controller.unit.unitMovementSpeed;
        }
    }
}