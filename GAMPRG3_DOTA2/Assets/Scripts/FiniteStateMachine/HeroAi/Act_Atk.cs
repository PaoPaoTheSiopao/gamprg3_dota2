﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Act_Atk")]
public class Act_Atk : Actions
{
    public override void Act(StateController controller)
    {
        Action(controller);
    }

    private void Action(StateController controller)
    {
        controller.unit.AttackTarget();
    }
}
