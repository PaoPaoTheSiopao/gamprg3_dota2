﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DireSide : MonoBehaviour
{
    public GameObject hero;
    public Transform spwPnt;
    public float spwPntRange;
    public Faction faction;

    public List<Transform> midPnts;
    public List<Transform> safePnts;
    public List<Transform> offPnts;
    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject h = Instantiate(hero, RandomSpawnPoint(), transform.rotation);
            h.GetComponent<Factions>().unitFaction = faction;
            h.GetComponent<HeroUnit>().heroID = i;
            GameManager.Instance.direTopHud.GetComponent<TopHud>().theHeroesHud[i].GetComponent<TopHeroPanel>().refHero = h;
            int w = Random.Range(1, 3);
            switch(w)
            {
                case 1:
                    h.GetComponent<HeroUnit>().unitPath = midPnts;
                    break;
                case 2:
                    h.GetComponent<HeroUnit>().unitPath = safePnts;
                    break;
                case 3:
                    h.GetComponent<HeroUnit>().unitPath = offPnts;
                    break;
            }
        }
    }

    void Update()
    {

    }
    private Vector3 RandomSpawnPoint()
    {
        Vector3 spwPntFoun = new Vector3(Random.Range(spwPnt.position.x - spwPntRange, spwPnt.position.x + spwPntRange), spwPnt.position.y, Random.Range(spwPnt.position.z - spwPntRange, spwPnt.position.z + spwPntRange));
        return spwPntFoun;
    }
}
