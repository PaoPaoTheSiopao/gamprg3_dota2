﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaScript : MonoBehaviour
{
    public float baseMana;
    public float totalMana;
    public float currentMana;

    private Stats stat;
    private void Start()
    {
        stat = GetComponent<Stats>();
    }
    private void Update()
    {
        if(currentMana> totalMana)
        {
            currentMana = totalMana;
        }
        else
        {
            if (GetComponent<Stats>() != null)
            {
                currentMana += stat.ReturnHpRegeneration() * Time.deltaTime;
            }
        }
    }
    public void UpdateMana()
    {
        totalMana = stat.ReturnTotalIntelligence() * 12 + baseMana;
    }
}
