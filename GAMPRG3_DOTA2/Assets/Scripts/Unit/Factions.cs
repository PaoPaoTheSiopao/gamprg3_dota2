﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Faction { Dire, Radiant, Neutral}

public class Factions : MonoBehaviour
{
    public Faction unitFaction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Faction ReturnFaction()
    {
        return unitFaction;
    }

    public bool IsNotAnAlly(GameObject target)
    {
        //UnitScript targetUnit = target.GetComponent<UnitScript>();
        Factions targetUnit = target.GetComponent<Factions>();
        //Debug.Log(targetUnit.ReturnFaction() + "!=" + this.ReturnFaction());
        if (targetUnit.ReturnFaction() != this.ReturnFaction())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
