﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlExpScript : MonoBehaviour
{
    public int currentLvl = 1;
    public float currentExp = 0;

    public int AvailSkillPoints = 0;

    public float baseDeathExp = 40;

    public float creepExp;

    public float[] expRequirements = { 230, 600, 1080, 1660, 2260, 2980, 3730, 4510, 5320, 6160, 7030, 7930, 9155, 10405, 11680, 12980, 14305, 15805, 17395, 18995, 20845, 22945, 25295, 27895 };
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateExp(float xpGained)
    {
        if (currentLvl != 25)
        {
            currentExp += xpGained;
            for (int i = currentLvl - 1; i < expRequirements.Length; i++)
            {
                if (currentExp >= expRequirements[i])
                {
                    currentLvl++;
                    GetComponent<Stats>().LvlUpGrowth();
                    if (currentLvl <= 18)
                    {
                        AvailSkillPoints++;
                    }
                }
            }
        }
    }
    public float ComputeDistribution(int dis)
    {
        float expDis = (baseDeathExp + 0.13f * currentExp) / dis;
        //Debug.Log("Computing Distri");
        return expDis;
    }
}
