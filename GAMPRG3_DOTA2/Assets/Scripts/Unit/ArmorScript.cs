﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorScript : MonoBehaviour
{
    public float baseArmor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public float ReturnDamageReduction()
    {
        if (GetComponent<Stats>() != null)
        {
            float reduct = 1 - ((0.052f * GetComponent<Stats>().ReturnArmor(baseArmor)) / (0.9f + 0.048f * Mathf.Abs(GetComponent<Stats>().ReturnArmor(baseArmor))));
            //Debug.Log("Reduction : " + reduct);
            return reduct;
        }
        else
        {
            float reduct = 1 - ((0.052f * baseArmor) / (0.9f + 0.048f * Mathf.Abs(baseArmor)));
            //Debug.Log("Reduction : " + reduct);
            return reduct;
        }
    }
}
