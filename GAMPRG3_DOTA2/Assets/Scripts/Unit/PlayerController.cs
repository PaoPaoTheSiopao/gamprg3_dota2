﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    Factions fact;
    HeroUnit myUnit;
    void Start()
    {
        fact = GetComponent<Factions>();
        myUnit = GetComponent<HeroUnit>();
    }

    void Update()
    {

        //Debug.Log("Target : "+myUnit.target);
        if (myUnit.target != null)
        {
            //Debug.Log("X : " + myUnit.target.transform.position.x + " Y : " + myUnit.target.transform.position.y + " Z : " + myUnit.target.transform.position.z);//myUnit.target.transform.position.y-(myUnit.target.transform.localScale.y)
            myUnit.unitAgent.SetDestination(new Vector3(myUnit.target.transform.position.x, myUnit.target.transform.position.y - (myUnit.target.transform.localScale.y/2), myUnit.target.transform.position.z));
            if (fact.IsNotAnAlly(myUnit.target))
            {
                //Debug.Log("not an ally");
                this.GetComponent<UnitScript>().AttackTarget();
            }
        }
        if(Input.GetMouseButtonDown(1))
        {
            myUnit.target = null;
            myUnit.unitAgent.isStopped = false;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 100);
            if (hit.collider.gameObject.layer == 9)
            {
                this.GetComponent<UnitScript>().MoveUnitToLocation(hit);
            }
            else if (hit.collider.gameObject.layer == 10 || hit.collider.gameObject.layer == 11) 
            {
                this.GetComponent<UnitScript>().MoveUnitToTarget(hit);
                //Debug.Log("Clicked : " + hit.collider.name);
            }
            else
            {
                //Debug.Log("Invalid Layer");
            }
        }
        if(Input.GetKeyDown(KeyCode.S))
        {

            myUnit.unitAgent.velocity = Vector3.zero;
            myUnit.target = null;
            myUnit.unitAgent.ResetPath();
        }
    }


    public void UpdateRotation()
    {
        Vector3 direction = myUnit.unitAgent.destination - transform.position;
        //Vector3 newDir = Vector3.RotateTowards(this.transform.forward, direction, rotSpeed * Time.deltaTime, 0.0f);
        //transform.rotation = Quaternion.LookRotation(newDir);
    }
}
