﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class UnitScript : MonoBehaviour
{
    public GameObject target;

    public FieldOfVision fov;
    public List<Transform> unitPath;
    public NavMeshAgent unitAgent;
    public NavMeshPath agentPath;
    public Factions faction;
    public int pathIndex = 0;

    public float baseAttackTime;
    public float increasedAttackSpeed;
    public float attackPerSecond;

    public float attackRange;
    public float unitMovementSpeed;
    public float damage;
    public float chaseRange;

    public float baseXpGive;

    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        unitAgent = GetComponent<NavMeshAgent>();
        unitAgent.updateRotation = false;
        fov = GetComponent<FieldOfVision>();
        faction = GetComponent<Factions>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Unit Update : " + this.gameObject.name);
    }

    public void NextWaypoint()
    {
        switch(faction.unitFaction)
        {
            case Faction.Dire:
                if (pathIndex > 0)
                {
                    pathIndex--;
                    unitAgent.SetDestination(unitPath[pathIndex].position);
                    if (pathIndex > 2)
                    {
                        NavMesh.CalculatePath(unitPath[pathIndex].position, unitPath[pathIndex-1].position,NavMesh.AllAreas,agentPath);
                    }
                }
                break;
            case Faction.Radiant:
                if (pathIndex != unitPath.Count - 1)
                {
                    pathIndex++;
                    unitAgent.SetDestination(unitPath[pathIndex].position);
                }
                break;
        }
    }

    public void FindNearestWayPoint()
    {
        int lowest = 0;
        for (int i = 1; i < unitPath.Count; i++)
        {
            Vector3 one = unitPath[lowest].position - this.transform.position;
            Vector3 two = unitPath[i].position - this.transform.position;
            //Debug.Log("Point "+unitPath[i].name+" : " + two.magnitude);
            if (one.magnitude > two.magnitude)
            {
                lowest = i;
            }
        }
        //Debug.Log("Nearest Point : " + unitPath[lowest].name);
        unitAgent.SetDestination(unitPath[lowest].position);
        pathIndex = lowest;
    }
    public int ReturnNearestWayPoint()
    {
        int lowest = 0;
        for (int i = 1; i < unitPath.Count; i++)
        {
            Vector3 one = unitPath[lowest].position - this.transform.position;
            Vector3 two = unitPath[i].position - this.transform.position;
            //Debug.Log("Point "+unitPath[i].name+" : " + two.magnitude);
            if (one.magnitude > two.magnitude)
            {
                lowest = i;
            }
        }
        return lowest;
    }

    public bool ScanForEnemies()
    {
        for (int i = 0; i < fov.visiblePlayer.Count; i++)
        {
            if (fov.visiblePlayer[i].gameObject != null)
            {
                GameObject unit = fov.visiblePlayer[i].gameObject;
                if (faction.IsNotAnAlly(unit.gameObject))
                {
                    if (unit.GetComponent<HealthScrpStruc>() != null && !unit.GetComponent<HealthScrpStruc>().isInvulnerable)
                    {
                        target = unit.gameObject;
                        return true;
                    }
                    else if (unit.GetComponent<HealthScript>() != null && !unit.GetComponent<HealthScript>().isInvulnerable)
                    {
                        target = unit.gameObject;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    //Debug.Log("Returned True");
                }
                else
                {
                    //Debug.Log("Returned False");
                    return false;
                }
            }
        }
        return false;
    }

    public void AttackTarget()
    {
        if (target != null)
        {
            //Debug.Log("Distance = " + Vector3.Distance(new Vector3(this.transform.position.x, 0, this.transform.position.z), new Vector3(target.transform.position.x, 0, target.transform.position.z)));
            if (Vector3.Distance(new Vector3(this.transform.position.x, 0, this.transform.position.z), new Vector3(target.transform.position.x, 0, target.transform.position.z)) < attackRange)
            {
                unitAgent.velocity = Vector3.zero;
                unitAgent.isStopped = true;
                if (attackPerSecond <= 0)
                {
                    //Debug.Log("Attack Target");
                    DamageTarget();
                    attackPerSecond = ReturnAttackPerSecond();
                }
                else
                {
                    attackPerSecond -= Time.deltaTime;
                }
            }
            else
            {
                unitAgent.isStopped = false;
                attackPerSecond = ReturnAttackPerSecond();
            }
        }
        else
        {
            unitAgent.isStopped = false;
            unitAgent.ResetPath();
        }
    }

    public virtual void DamageTarget()
    {
        if(target.GetComponent<HealthScript>()!=null)
        {
            target.GetComponent<HealthScript>().TakeDamage(damage,this.gameObject);
            if(target==null)
            {
                unitAgent.isStopped = false;
            }
        }
        else if(target.GetComponent<HealthScrpStruc>()!=null)
        {
            target.GetComponent<HealthScrpStruc>().TakeDamage(damage,this.gameObject);
            if (target == null)
            {
                unitAgent.isStopped = false;
            }
        }
    }

    public float ReturnAttackPerSecond()
    {
        if (GetComponent<Creeps>() != null)
        {
            return 1;
        }
        else
        {
            return GetComponent<Stats>().ReturnAttackSpeed();
        }
    }
    public void FollowTargetUnit()
    {
        if (target != null)
        {
            unitAgent.SetDestination(new Vector3(target.transform.position.x, target.transform.position.y - 1, target.transform.position.z));
        }
    }

    public void MoveUnitToLocation(RaycastHit mHit)
    {
        unitAgent.destination = mHit.point;
        //Debug.Log("Raycast = " + mHit.collider.gameObject.layer);
    }

    public Vector3 ReturnAngleBetween(Vector3 tar)
    {
        direction = tar - transform.position;
        var foward = transform.forward;
        float angle = Vector3.Angle(direction, foward);
        /*
        if (Vector3.Cross(foward, direction).y < 0)
        {
            return angle * -1;
        }
        else
        {
            return angle * 1;
        }
        */
        return new Vector3(direction.x, direction.y + transform.localScale.y, direction.z);
    }

    public void MoveUnitToTarget(RaycastHit mHit)
    {
        if (mHit.collider.GetComponent<HealthScript>()!=null)
        {
            if(mHit.collider.GetComponent<HealthScript>().isInvulnerable)
            {
                //Debug.Log("Invulnerable");
                return;
            }
            else
            {
                //Debug.Log("NotInvul");
                target = mHit.collider.gameObject;
            }
        }
        else if (mHit.collider.GetComponent<HealthScrpStruc>() != null)
        {
            if (mHit.collider.GetComponent<HealthScrpStruc>().isInvulnerable)
            {
                //Debug.Log("Invulnerable");
                return;
            }
            else
            {
                //Debug.Log("NotInvul");
                target = mHit.collider.gameObject;
            }
        }
        //Debug.Log("Raycast = " + mHit.collider.gameObject.name);
    }

}
