﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BountyScript : MonoBehaviour
{
    public float totalGold;
    public float baseGoldBounty;
    public Vector2 creepBounty;
    public Vector2 towerBounty;
    private float timer = 0.7f;
    // Start is called before the first frame update
    void Start()
    {
        if(GetComponent<HeroUnit>()!=null)
        {
            totalGold = 600;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<HeroUnit>() != null)
        {
            if(timer<=0)
            {
                totalGold += 1;
                timer = 0.7f;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
    }

    public float ReturnCreepBounty()
    {
        float b = Random.Range(creepBounty.x, creepBounty.y);
        return b;
    }

    public void SetCreepBounty(Vector2 b)
    {
        creepBounty = b;
    }
}
