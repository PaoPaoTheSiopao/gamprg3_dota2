﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    public float baseHealth;
    public float currentHealth;
    public float totalHealth;

    public LayerMask playerMask;

    public GameObject floatingText;
    public GameObject respawner;

    private Stats stat;


    public bool isInvulnerable;
    // Start is called before the first frame update
    void Start()
    {
        stat = GetComponent<Stats>();
    }

    // Update is called once per frame
    void Update()
    {

        if (GetComponent<Stats>() != null)
        {
            UpdateHealth();
        }
        if (currentHealth>totalHealth)
        {
            currentHealth = totalHealth;
        }
        else
        {
            if (GetComponent<Stats>() != null)
            {
                currentHealth += stat.ReturnHpRegeneration() * Time.deltaTime;
            }
        }
    }

    public void UpdateCreepHealth(float tothel)
    {
        Debug.Log("ScaleHP : " + tothel);
        baseHealth += tothel * 12;
        totalHealth = baseHealth;
        currentHealth = baseHealth;
    }

    public void FullHeal()
    {
        currentHealth = totalHealth;
    }
    public void CreepInitialHealth(float health)
    {
        baseHealth = health;
        totalHealth = health;
        currentHealth = health;
    }

    public void UpdateHealth()
    {
        totalHealth = (stat.ReturnTotalStrength() * 20) + baseHealth;
    }

    public void TakeDamage(float damage, GameObject damager)
    {
        currentHealth -= damage * GetComponent<ArmorScript>().ReturnDamageReduction();
        
        if (floatingText != null)
        {
            ShowFloatingText(damage);
        }
        if(currentHealth<=0)
        {
            Die(damager);
        }
    }

    public void Die(GameObject damager)
    {
        if(GetComponent<Creeps>()!=null)
        {
            damager.GetComponent<BountyScript>().totalGold += GetComponent<BountyScript>().ReturnCreepBounty();
            Collider[] dist = Physics.OverlapSphere(this.transform.position, 30, playerMask);
            List<GameObject> distTo = new List<GameObject>();
            //Debug.Log("dist : " + dist.Length);
            for (int i = 0; i < dist.Length; i++)
            {
                if (dist[i].GetComponent<HeroUnit>() != null)
                {
                    if (this.GetComponent<Factions>().unitFaction != dist[i].GetComponent<Factions>().unitFaction)
                    {
                        distTo.Add(dist[i].gameObject);
                        //Debug.Log("Added : " + dist[i].name);
                    }
                }
            }
            for (int z = 0; z < distTo.Count; z++)
            {
                distTo[z].GetComponent<LvlExpScript>().UpdateExp(GetComponent<LvlExpScript>().ComputeDistribution(distTo.Count));
            }
            Destroy(this.gameObject);
        }
        else if(GetComponent<HeroUnit>()!=null)
        {
            damager.GetComponent<BountyScript>().totalGold += (100 + (GetComponent<LvlExpScript>().currentLvl * 8));
            //Debug.Log("Damager : "+ damager.name);
            Collider[] dist = Physics.OverlapSphere(this.transform.position, 30,playerMask);
            List<GameObject> distTo = new List<GameObject>();
            //Debug.Log("dist : "+dist.Length);
            for (int i = 0;i<dist.Length;i++)
            {
                if(dist[i].GetComponent<HeroUnit>()!=null)
                {
                    if(this.GetComponent<Factions>().unitFaction!=dist[i].GetComponent<Factions>().unitFaction)
                    {
                        distTo.Add(dist[i].gameObject);
                        //Debug.Log("Added : "+dist[i].name);
                    }
                }
            }
            for(int z = 0;z<distTo.Count;z++)
            {
                distTo[z].GetComponent<LvlExpScript>().UpdateExp(GetComponent<LvlExpScript>().ComputeDistribution(distTo.Count));
            }
            if(damager.GetComponent<HeroUnit>()!=null)
            {
                damager.GetComponent<HeroUnit>().target = null;
            }
            else if (damager.GetComponent<Creeps>() != null)
            {
                damager.GetComponent<Creeps>().target = null;
            }
            StoreHero(this.gameObject);
        }
    }
    public void ShowFloatingText(float damage)
    {
        var go = Instantiate(floatingText, new Vector3(this.transform.position.x, this.transform.position.y+2, this.transform.position.z), Quaternion.identity, transform);
        go.GetComponent<TextMesh>().text = damage.ToString();
    }

    public void StoreHero(GameObject hero)
    {
        //Debug.Log("Store : " + gameObject.name);
        GameObject res = Instantiate(respawner, transform.position, transform.rotation);
        HeroRespawner pawn = res.GetComponent<HeroRespawner>();
        pawn.theHero = hero;
        pawn.whatFact = hero.GetComponent<Factions>().unitFaction;
        FullHeal();
        hero.gameObject.SetActive(false);
        pawn.timer = 30;
    }

    public void KillHero(GameObject hero)
    {
        //Debug.Log("Store : " + gameObject.name);
        GameObject res = Instantiate(respawner, transform.position, transform.rotation);
        HeroRespawner pawn = res.GetComponent<HeroRespawner>();
        pawn.theHero = hero;
        pawn.whatFact = hero.GetComponent<Factions>().unitFaction;
        FullHeal();
        hero.gameObject.SetActive(false);
        pawn.timer = 30;
    }
}
