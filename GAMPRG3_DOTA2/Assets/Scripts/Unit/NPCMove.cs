﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMove : MonoBehaviour
{

    [SerializeField]
    Transform destination;

    NavMeshAgent navmeshAgent;


    // Start is called before the first frame update
    void Start()
    {
        navmeshAgent = GetComponent<NavMeshAgent>();

        SetDestination();
    }

    // Update is called once per frame
    void Update()
    {
        if (navmeshAgent.remainingDistance < 0.5f)
        {
            Debug.Log("Path Complete");
        }
        else if(navmeshAgent.hasPath)
        {
            Vector3 toTarget = navmeshAgent.steeringTarget - this.transform.position;
            float turnAngle = Vector3.Angle(this.transform.forward, toTarget);
            navmeshAgent.acceleration = turnAngle * navmeshAgent.speed;
        }
    }

    void SetDestination()
    {
        if(destination!=null)
        {
            Vector3 targetVector = destination.transform.position;
            navmeshAgent.SetDestination(targetVector);
            
        }
    }

}
