﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HeroUnit : UnitScript
{
    public int heroID;
    private void Start()
    {
        unitAgent = GetComponent<NavMeshAgent>();
        unitAgent.updateRotation = false;
        fov = GetComponent<FieldOfVision>();
        faction = GetComponent<Factions>();
        attackPerSecond = ReturnAttackPerSecond();
        if (GetComponent<PlayerController>() == null)
        {
            switch (faction.unitFaction)
            {
                case Faction.Dire:
                    pathIndex = unitPath.Count - 1;
                    break;
                case Faction.Radiant:
                    pathIndex = 0;
                    break;
            }
            unitAgent.SetDestination(unitPath[pathIndex].position);
        }
    }
    private void Update()
    {
        if(unitAgent.hasPath)
        {
            transform.rotation = Quaternion.LookRotation(ReturnAngleBetween(unitAgent.path.corners[1]));
        }
    }

    public override void DamageTarget()
    {
        if (target.GetComponent<HealthScript>() != null)
        {
            target.GetComponent<HealthScript>().TakeDamage(GetComponent<Stats>().ReturnDamage(), this.gameObject);
            if (target == null)
            {
                unitAgent.isStopped = false;
            }
        }
        else if (target.GetComponent<HealthScrpStruc>() != null)
        {
            target.GetComponent<HealthScrpStruc>().TakeDamage(GetComponent<Stats>().ReturnDamage(), this.gameObject);
            if (target == null)
            {
                unitAgent.isStopped = false;
            }
        }
    }

    public void ResetPath()
    {
        if (GetComponent<PlayerController>() == null)
        {
            switch (faction.unitFaction)
            {
                case Faction.Dire:
                    pathIndex = unitPath.Count - 1;
                    break;
                case Faction.Radiant:
                    pathIndex = 0;
                    break;
            }
            unitAgent.SetDestination(unitPath[pathIndex].position);
        }
    }
    
}
