﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfVision : MonoBehaviour
{

    public float viewRadius;
    Collider[] playerInRadius;
    [SerializeField]public LayerMask obstacleMask, playerMask;
    public List<Transform> visiblePlayer = new List<Transform>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
    }
    private void Start()
    {
        
    }
    private void FixedUpdate()
    {
        FindPlayer();
    }

    public Vector2 DirFromAngle(float angleDeg, bool global)
    {
        if (!global)
        {
            angleDeg += transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Sin(angleDeg * Mathf.Deg2Rad), Mathf.Cos(angleDeg * Mathf.Deg2Rad));
    }

    public void FindPlayer()
    {
        playerInRadius = Physics.OverlapSphere(transform.position,viewRadius,playerMask);
        //Debug.Log(playerInRadius.Length);
        visiblePlayer.Clear();

        for(int i = 0;i< playerInRadius.Length;i++)
        {
            Transform player = playerInRadius[i].transform;
            if (player != this.transform)
            {
                //if(GetComponent<Factions>().unitFaction!=player.gameObject.GetComponent<Factions>().unitFaction)
                visiblePlayer.Add(player);
            }
        }
    }
}
