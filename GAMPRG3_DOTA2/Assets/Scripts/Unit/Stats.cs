﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PrimaryAttribute { Strength,Agility,Intelligence };
public class Stats : MonoBehaviour
{
    public float baseStrength;
    public float modStrength;
    private float totalStrength;

    public float baseAgility;
    public float modAgility;
    private float totalAgility;

    public float baseIntelligence;
    public float modIntelligence;
    private float totalIntelligence;

    public int modArmor;

    public float hpRegeneration;
    public float mpRegeneration;

    //public float attackRange;
    //public float movementSpeed;
    public PrimaryAttribute primeAtt;

    public Vector3 statGrowth;

    public float baseDamage;
    public float heroBaseDamage;
    private float primeDamage;
    public float modDamage;
    public float totalDamage;
    // Start is called before the first frame update
    void Start()
    {
        UpdateDamage();
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LvlUpGrowth()
    {
        baseStrength += statGrowth.x;
        baseAgility += statGrowth.y;
        baseIntelligence += statGrowth.z;
    }

    public float ReturnArmor(float baseArm)
    {
        if (GetComponent<UnitScript>() != null)
        {
            float totalArmor = totalAgility * 0.16f + baseArm + modArmor;
            return totalArmor;
        }
        else
        {
            float totalArmor = totalAgility * 0.16f + baseArm + modArmor;
            return totalArmor;
        }
    }
    public float ReturnTotalAgility()
    {
        totalAgility = modAgility + baseAgility;
        return totalAgility;
    }
    public float ReturnHpRegeneration()
    {
        hpRegeneration = totalStrength * 0.1f;
        return hpRegeneration;
    }
    public float ReturnMpRegeneration()
    {
        mpRegeneration = totalIntelligence * 0.05f;
        return mpRegeneration;
    }

    public void UpdateDamage()
    {
        switch(primeAtt)
        {
            case PrimaryAttribute.Agility:
                primeDamage = totalAgility;
                heroBaseDamage = primeDamage + baseDamage;
                break;
            case PrimaryAttribute.Strength:
                primeDamage = totalStrength;
                heroBaseDamage = primeDamage + baseDamage;
                break;
            case PrimaryAttribute.Intelligence:
                primeDamage = totalIntelligence;
                heroBaseDamage = primeDamage + baseDamage;
                break;
        }
    }

    public float ReturnDamage()
    {
        UpdateDamage();
        totalDamage = heroBaseDamage + modDamage;
        return totalDamage;
    }

    public float ReturnAttackSpeed()
    {
        GetComponent<UnitScript>().increasedAttackSpeed = ReturnTotalAgility() * 1;
        float aPS = ((100 + GetComponent<UnitScript>().increasedAttackSpeed) * 0.01f) / GetComponent<UnitScript>().baseAttackTime;
        return aPS;
    }

    public float ReturnTotalStrength()
    {
        totalStrength = baseStrength + modStrength;
        return totalStrength;
    }

    public float ReturnTotalIntelligence()
    {
        totalIntelligence = baseIntelligence + modIntelligence;
        return totalIntelligence;
    }
}
