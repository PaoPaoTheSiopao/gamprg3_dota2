﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshTestScript : MonoBehaviour
{
    NavMeshAgent agent;

    public Vector3[] corners;

    public float angle;
    public GameObject target;
    Vector3 dir;
    float sign = 1;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //corners = agent.path.corners;
        ReturnAngleBetween();
        Debug.Log(angle);
    }

    public void ReturnAngleBetween()
    {
        dir = target.transform.position - transform.position;
        var foward = transform.forward;
        angle = Vector3.Angle(dir, foward); 
        if (Vector3.Cross(foward, dir).y < 0)
        {
            angle *= -1;
        }
        else
        {
            angle *= 1;
        }
    }
    public void CheckAngle2()
    {
        Vector2 dir = new Vector2(this.transform.position.x, this.transform.position.z) - new Vector2(target.transform.position.x, target.transform.position.z);
        sign = (dir.y >= 0) ? 1 : -1;
        angle = Vector2.Angle(Vector2.up, dir)*sign;
    }
}
