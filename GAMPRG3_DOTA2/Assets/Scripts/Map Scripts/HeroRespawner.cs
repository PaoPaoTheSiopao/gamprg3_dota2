﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroRespawner : MonoBehaviour
{
    public GameObject theHero;

    public float timer;

    private Text text;
    private GameObject textHud;
    public Faction whatFact;
    // Start is called before the first frame update
    void Start()
    {
        switch (whatFact)
        {
            case Faction.Dire:
                for (int i = 0; i < GameManager.Instance.direTopHud.GetComponent<TopHud>().theHeroesHud.Count; i++)
                {
                    if (theHero.GetComponent<HeroUnit>().heroID == GameManager.Instance.direTopHud.GetComponent<TopHud>().theHeroesHud[i].GetComponent<TopHeroPanel>().refHero.GetComponent<HeroUnit>().heroID)
                    {
                        textHud = GameManager.Instance.direTopHud.GetComponent<TopHud>().theHeroesHud[i].GetComponent<TopHeroPanel>().heroText;
                        textHud.SetActive(true);
                        text = textHud.GetComponent<Text>();
                    }
                }
                break;
            case Faction.Radiant:
                for (int i = 0; i < GameManager.Instance.radiantTopHud.GetComponent<TopHud>().theHeroesHud.Count;i++)
                {
                    if(theHero.GetComponent<HeroUnit>().heroID == GameManager.Instance.radiantTopHud.GetComponent<TopHud>().theHeroesHud[i].GetComponent<TopHeroPanel>().refHero.GetComponent<HeroUnit>().heroID)
                    {
                        textHud = GameManager.Instance.radiantTopHud.GetComponent<TopHud>().theHeroesHud[i].GetComponent<TopHeroPanel>().heroText;
                        textHud.SetActive(true);
                        text = textHud.GetComponent<Text>();
                    }
                }
                    break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(timer<=0)
        {
            RespawnHero();
            textHud.SetActive(false);
            Destroy(this.gameObject);
        }
        else
        {
            //Debug.Log("Text : " + text);
            text.text = timer.ToString("F0");
            timer -= Time.deltaTime;
        }
    }

    public void RespawnHero()
    {
        switch(whatFact)
        {
            case Faction.Dire:
                theHero.transform.position = GameManager.Instance.direSpawn.position;
                theHero.SetActive(true);
                theHero.GetComponent<HeroUnit>().ResetPath();
                break;
            case Faction.Radiant:
                theHero.transform.position = GameManager.Instance.radiantSpawn.position;
                theHero.SetActive(true);
                theHero.GetComponent<HeroUnit>().ResetPath();
                break;
        }
    }
}
