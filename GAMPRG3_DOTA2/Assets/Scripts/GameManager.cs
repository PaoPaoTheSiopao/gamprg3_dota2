﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public float GameTime;
    public static GameManager instance;

    public GameObject selectedChar;
    public int timeMultiplier = 1;

    public Transform radiantSpawn;
    public Transform direSpawn;

    public GameObject radiantTopHud;
    public GameObject direTopHud;
    // Start is called before the first frame update
    private void Awake()
    {
        selectedChar = GameObject.FindGameObjectWithTag("Player");
        GameTime = -10;
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Time = " + GetGameTime());
        UpdateGameTime();
        
        if (Input.GetKey(KeyCode.Space))
        {
            timeMultiplier = 10;
        }
        else if(Input.GetKey(KeyCode.Backspace))
        {
            timeMultiplier = -10;
        }
        else
        {
            timeMultiplier = 1;
        }
    }

    private void UpdateGameTime()
    {
        GameTime += timeMultiplier * Time.deltaTime;
    }

    public int GetGameTime()
    {
        return (int)GameTime;
    }

    
}
