﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAttributes : MonoBehaviour
{
    private Stats stat;
    public GameObject player;

    public Text Damage;
    public Text Armor;
    public Text Strength;
    public Text Agility;
    public Text Intelligence;
    public Text MovementSpeed;
    public Text Gold;
    public Text Level;
    public Text Experience;

    public GameObject hpBar;
    public GameObject mpBar;
    private void Awake()
    {
    }
    void Start()
    {
        stat = player.GetComponent<Stats>();
        UpdateUIStats();
    }

    // Update is called once per frame
    void Update()
    {
        hpBar.GetComponent<Image>().fillAmount = player.GetComponent<HealthScript>().currentHealth / player.GetComponent<HealthScript>().totalHealth;
        UpdateUIStats();
    }
    public void UpdateUIStats()
    {
        Damage.text ="Damage : "+ stat.heroBaseDamage + " + " + stat.modDamage;
        Armor.text = "Armor : "+ player.GetComponent<ArmorScript>().ReturnDamageReduction() + " + " + stat.modArmor;
        Strength.text = "STR : "+stat.baseStrength + " + " + stat.modStrength;
        Agility.text = "AGI : "+stat.baseAgility + " + " + stat.modAgility;
        Intelligence.text = "INT : "+stat.baseIntelligence + " + " + stat.modIntelligence;
        MovementSpeed.text = "MS : " + player.GetComponent<HeroUnit>().unitMovementSpeed;
        Gold.text = "Gold : " + player.GetComponent<BountyScript>().totalGold;
        Level.text = " Level : " + player.GetComponent<LvlExpScript>().currentLvl;
        Experience.text = " XP : " + player.GetComponent<LvlExpScript>().currentExp;
    }

    public void UpdatePlayer()
    {
        player = GameManager.instance.selectedChar;
        stat = player.GetComponent<Stats>();
    }
}
