﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float destroyTime = 1.5f; 
    void Start()
    {
        transform.eulerAngles = new Vector3(45, -90, 0);
        Destroy(gameObject, destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0, 1 * Time.deltaTime, 0));
    }
}
