﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvasScript : MonoBehaviour
{
    public GameObject stateBanner;
    public GameObject timer;

    private int dBarrackCounter = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
    }

    public void UpdateTimer()
    {
        string minutes = (GameManager.Instance.GetGameTime() / 60).ToString();
        string seconds = (GameManager.Instance.GameTime % 60).ToString("F2");

        timer.GetComponent<Text>().text = minutes + ":" + seconds;
    }

    public void KillAllEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Hero");
        for(int i = 0;i<enemies.Length;i++)
        {
            if(enemies[i].GetComponent<Factions>().unitFaction != Faction.Radiant)
            {
                enemies[i].GetComponent<HealthScript>().KillHero(enemies[i]);
            }
        }
    }

    public void LvlUpHero()
    {
        GameObject hero = GameObject.FindGameObjectWithTag("Player");
        LvlExpScript lvlXp = hero.GetComponent<LvlExpScript>();
        if (lvlXp.currentLvl < 25)
        {
            lvlXp.currentExp = lvlXp.expRequirements[lvlXp.currentLvl - 1];
            lvlXp.UpdateExp(0);
        }
    }

    public void DestroyAllBarracks()
    {
        if (dBarrackCounter == 1)
        {
            GameObject[] barracks = GameObject.FindGameObjectsWithTag("Barrack");
            for (int i = 0; i < barracks.Length; i++)
            {
                barracks[i].GetComponent<HealthScrpStruc>().DestroyBarrack();

            }
            for (int i = 0; i < barracks.Length; i++)
            {
                barracks[i].GetComponent<MeshRenderer>().enabled = false;
                barracks[i].GetComponent<BoxCollider>().enabled = false;
            }
            dBarrackCounter = 0;
        }

    }
    public void DestroyTierTowers(string tier)
    {
        GameObject[] towers = GameObject.FindGameObjectsWithTag(tier);
        for (int i = 0; i < towers.Length; i++)
        {
            towers[i].GetComponent<HealthScrpStruc>().DestroyTower();
        }
        for (int i = 0; i < towers.Length; i++)
        {
            Destroy(towers[i].gameObject);
        }
    }
}
