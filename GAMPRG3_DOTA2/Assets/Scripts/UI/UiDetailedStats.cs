﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiDetailedStats : MonoBehaviour
{
    public GameObject player;
    private Stats sta;
    private ArmorScript armor;
    private HeroUnit unit;
    private HealthScript hp;
    private ManaScript mp;


    public Text AttackSpeed;
    public Text Damage;
    public Text AttackRange;
    public Text MoveSpeed;
    public Text SpellAmp;
    public Text ManaRegen;

    public Text Armor;
    public Text PhyRes;
    public Text MagRes;
    public Text StaRes;
    public Text Evasion;
    public Text HealthRegen;

    public Text Strength;
    public Text Agility;
    public Text Intelligence;
    // Start is called before the first frame update
    void Start()
    {
        sta = player.GetComponent<Stats>();
        armor = player.GetComponent<ArmorScript>();
        unit = player.GetComponent<HeroUnit>();
        hp = player.GetComponent<HealthScript>();
        mp = player.GetComponent<ManaScript>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDetailedUIStats();
    }

    public void UpdateDetailedUIStats()
    {
        AttackSpeed.text = "Attack Speed: " + (unit.increasedAttackSpeed + 100 + (sta.ReturnTotalAgility() * 1)) + " (" + unit.ReturnAttackPerSecond() + ")";
        Damage.text = "Damage: " + unit.damage;
        AttackRange.text = "Attack Range: " + unit.attackRange * 16;
        MoveSpeed.text = "Movement Speed: " + unit.unitMovementSpeed * 16;
        SpellAmp.text = "Spell Amplification: " + "NaN";
        ManaRegen.text = "Mana Regeneration: " + sta.ReturnMpRegeneration();
        Armor.text = "Armor : " + (sta.ReturnArmor(armor.baseArmor) - sta.modArmor) + " + " + sta.modArmor;
        PhyRes.text = "Physical Ressistant: " + "NaN";
        MagRes.text = "Magic Ressistant: " + "NaN";
        StaRes.text = "Status Ressistant: " + "NaN";
        Evasion.text = "Evasion: " + "NaN";
        HealthRegen.text = "Health Regeneration: " + sta.ReturnHpRegeneration();

        Strength.text = sta.baseStrength + "+" + sta.modStrength + "(Gains " + sta.statGrowth.x + " per lvl)" + "\n" + "= " + hp.totalHealth + "HP"+", " + sta.ReturnHpRegeneration() + " HP Regen";
        Agility.text = sta.baseAgility + "+" + sta.modAgility + "(Gains " + sta.statGrowth.y + " per lvl)" + "\n" + "= " + sta.ReturnArmor(armor.baseArmor) +"Armor"+", " + sta.ReturnTotalAgility()*1 + " Attack Speed"+" and "+unit.unitMovementSpeed+"Movement Speed";
        Intelligence.text = sta.baseIntelligence + "+" + sta.modIntelligence + "(Gains " + sta.statGrowth.z + " per lvl" + "\n" + "= " + sta.ReturnTotalIntelligence()*12 + "Mana" + ", " + sta.ReturnMpRegeneration() + " Mana Regeneration";
    }
}
