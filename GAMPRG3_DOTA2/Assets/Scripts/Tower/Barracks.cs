﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : MonoBehaviour
{
    public GameManager gameManager;

    public List<Transform> pathPoints;

    public float spwPntRange;
    public int time;

    public Factions faction;

    public Barracks paralBarracks;

    public List<GameObject> creeps;
    public int spawnAmount;

    public int barrackLevel = 1;

    private int counter = 1;
    private void Awake()
    {
        faction = GetComponent<Factions>();
    }
    void Start()
    {/*
        for (int i = 0; i < 1; i++)
        {
            GameObject mCreep = Instantiate(meleeCreeps, RandomSpawnPoint(), transform.rotation);
            Creeps mCree = mCreep.GetComponent<Creeps>();
            mCree.SetParameters(pathPoints, faction);
        }*/
    }

    void Update()
    {
        time = GameManager.Instance.GetGameTime();
        if (time%30 == 0&&time>=0)
        {
            //Debug.Log("Time : "+ GameManager.Instance.GetGameTime());
            if (counter >= 1)
            {
                SpawnCreepsV2();
                counter = 0;
            }
        }
        else
        {
            counter = 1;
        }
    }

    private Vector3 RandomSpawnPoint()
    {
        Vector3 spwPnt = new Vector3(Random.Range(this.transform.position.x- spwPntRange, this.transform.position.x + spwPntRange), this.transform.position.y,Random.Range(this.transform.position.z-spwPntRange, this.transform.position.z+spwPntRange));
        return spwPnt;
    }

    /*
    private void SpawnCreeps()
    {
        switch(barrackLevel)
        {
            case 1:
                for (int i = 0; i < 3; i++)
                {
                    GameObject mCreep = Instantiate(meleeCreeps, RandomSpawnPoint(), transform.rotation);
                    Creeps mCree = mCreep.GetComponent<Creeps>();
                    mCree.SetParameters(pathPoints, faction);
                    Debug.Log("Path : " + pathPoints);
                    mCree.creepTyp = CreepTypes.Normal;
                }
                GameObject rCreep = Instantiate(rangeCreeps, transform.position, transform.rotation);
                Creeps rCree = rCreep.GetComponent<Creeps>();
                rCree.SetParameters(pathPoints, faction);
                rCree.creepTyp = CreepTypes.Normal;
                if (time % 300f == 0)
                {
                    GameObject sCreep = Instantiate(siegeCreeps, transform.position, transform.rotation);
                    Creeps sCree = sCreep.GetComponent<Creeps>();
                    sCree.SetParameters(pathPoints, faction);
                    sCree.creepTyp = CreepTypes.Normal;
                }
                break;
            case 2:
                for (int i = 0; i < 3; i++)
                {
                    GameObject mCreep = Instantiate(meleeCreeps, transform.position, transform.rotation);
                    Creeps mCree = mCreep.GetComponent<Creeps>();
                    mCree.SetParameters(pathPoints, faction);
                    mCree.creepTyp = CreepTypes.Super;
                }
                GameObject sRCreep = Instantiate(rangeCreeps, transform.position, transform.rotation);
                Creeps sRCree = sRCreep.GetComponent<Creeps>();
                sRCree.SetParameters(pathPoints, faction);
                sRCree.creepTyp = CreepTypes.Super;
                if (time % 30f == 0)
                {
                    GameObject sCreep = Instantiate(siegeCreeps, transform.position, transform.rotation);
                    Creeps sCree = sCreep.GetComponent<Creeps>();
                    sCree.SetParameters(pathPoints, faction);
                    sCree.creepTyp = CreepTypes.Super;
                }
                break;
            case 3:
                for (int i = 0; i < 3; i++)
                {
                    GameObject mCreep = Instantiate(meleeCreeps, transform.position, transform.rotation);
                    Creeps mCree = mCreep.GetComponent<Creeps>();
                    mCree.SetParameters(pathPoints, faction);
                    mCree.creepTyp = CreepTypes.Mega;
                }
                GameObject mRCreep = Instantiate(rangeCreeps, transform.position, transform.rotation);
                Creeps mRCree = mRCreep.GetComponent<Creeps>();
                mRCree.SetParameters(pathPoints, faction);
                mRCree.creepTyp = CreepTypes.Mega;
                if (time % 30f == 0)
                {
                    GameObject sCreep = Instantiate(rangeCreeps, transform.position, transform.rotation);
                    Creeps sCree = sCreep.GetComponent<Creeps>();
                    sCree.SetParameters(pathPoints, faction);
                    sCree.creepTyp = CreepTypes.Super;
                }
                break;
        }
        
    }
    *///SpawnCreepV1
    private void SpawnCreepsV2()
    {
        for (int i = 0; i < 1; i++)
        {
            for (int y = 0; y < spawnAmount; y++)
            {
                Debug.Log("Spawned : " + creeps[i].name);
                GameObject creep = Instantiate(creeps[i], RandomSpawnPoint(), transform.rotation);
                creep.GetComponent<Creeps>().SetParameters(pathPoints, faction);
                switch (barrackLevel)
                {
                    case 1:
                        creep.GetComponent<Creeps>().creepTyp = CreepTypes.Normal;
                        break;
                    case 2:
                        creep.GetComponent<Creeps>().creepTyp = CreepTypes.Super;
                        break;
                    case 3:
                        creep.GetComponent<Creeps>().creepTyp = CreepTypes.Mega;
                        break;
                }
                if (time % 300f == 0&&time>0&&creeps.Count>1)
                {
                    GameObject screep = Instantiate(creeps[1], RandomSpawnPoint(), transform.rotation);
                    screep.GetComponent<Creeps>().SetParameters(pathPoints, faction); 
                    switch (barrackLevel)
                    {
                        case 1:
                            screep.GetComponent<Creeps>().creepTyp = CreepTypes.Normal;
                            break;
                        case 2:
                            screep.GetComponent<Creeps>().creepTyp = CreepTypes.Super;
                            break;
                        case 3:
                            screep.GetComponent<Creeps>().creepTyp = CreepTypes.Mega;
                            break;
                    }
                }
                else
                {
                    //return;
                }
            }
        }
    }
}
