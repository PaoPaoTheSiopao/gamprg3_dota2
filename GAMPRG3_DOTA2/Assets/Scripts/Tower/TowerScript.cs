﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    Transform target;
    FieldOfVision fov;
    Factions towerFaction;

    public Transform projSpawnPoint;
    public GameObject projectile;

    //
    public List<GameObject> linkedTowers;

    public Vector2 damageRange;

    public float fireRate = 5f;
    private float defRate;
    void Start()
    {
        fov = GetComponent<FieldOfVision>();
        towerFaction = GetComponent<Factions>();
        defRate = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(target != null)
        {
            if(fireRate<=0)
            {
                FireProjectile();
                fireRate = defRate;
            }
            else
            {
                fireRate -= Time.deltaTime;
            }
            if(Vector3.Distance(this.transform.position,target.transform.position)>5)
            {
                target = null;
            }
        }
        else
        {
            if (GetComponent<FieldOfVision>() != null)
            {
                ScanForEnemies();
            }
        }
    }

    public void ScanForEnemies()
    {
        //Debug.Log("Scanning for Enemies");
        if (fov.visiblePlayer.Count == 0)
        {
            return;
        }
        else if (fov.visiblePlayer.Count==1)
        {
            if (towerFaction.IsNotAnAlly(fov.visiblePlayer[0].gameObject)&& fov.visiblePlayer[0].gameObject!=null)
            {
                //Debug.Log("NotAnAlly");
                target = fov.visiblePlayer[0];
            }
        }
        else
        {
            int lowest = 0;
            for (int i = 1; i < fov.visiblePlayer.Count; i++)
            {
                if (towerFaction.IsNotAnAlly(fov.visiblePlayer[i].gameObject))
                {
                    Vector3 one = fov.visiblePlayer[lowest].position - this.transform.position;
                    Vector3 two = fov.visiblePlayer[i].position - this.transform.position;
                    if (one.magnitude > two.magnitude)
                    {
                        lowest = i;
                    }
                }
                
            }
            if(towerFaction.IsNotAnAlly(fov.visiblePlayer[lowest].gameObject))
            {
                target = fov.visiblePlayer[lowest];
            }
        }
    }
    public bool ScanForEnemiesV()
    {
        for (int i = 0; i < fov.visiblePlayer.Count; i++)
        {
            GameObject unit = fov.visiblePlayer[i].gameObject;
            if (towerFaction.IsNotAnAlly(unit.gameObject))
            {
                target = unit.gameObject.transform;
                //Debug.Log("Returned True");
                return true;
            }
            else
            {
                //Debug.Log("Returned False");
                return false;
            }
        }
        return false;
    }

    public void FireProjectile()
    {
        GameObject proj = Instantiate(projectile, projSpawnPoint.position, this.transform.rotation);
        proj.GetComponent<TowerProjectile>().target = target;
        proj.GetComponent<TowerProjectile>().origin = this.gameObject;
        proj.GetComponent<TowerProjectile>().damage = Random.Range(damageRange.x, damageRange.y);
    }
}
