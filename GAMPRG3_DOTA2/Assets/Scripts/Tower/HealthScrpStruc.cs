﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScrpStruc : MonoBehaviour
{
    public float totalHP;
    public float currentHp;
    public GameObject floatingText;
    private Stats stat;

    public bool isInvulnerable;
    private void Start()
    {
        stat = GetComponent<Stats>();
    }

    void Update()
    {
        if (currentHp > totalHP)
        {
            currentHp = totalHP;
        }
        else
        {
        }
    }

    public void TakeDamage(float damage,GameObject damager)
    {
        currentHp -= damage * GetComponent<ArmorScript>().ReturnDamageReduction();
        //Debug.Log("Took Damage : " + damage * stat.ReturnDamageReduction()); 
        if (floatingText != null)
        {
            ShowFloatingText(damage * GetComponent<ArmorScript>().ReturnDamageReduction());
        }
        if (currentHp<=0)
        {
            DestroyStructure(damager);
        }
    }

    public void DestroyStructure(GameObject damager)
    {
        if (GetComponent<BountyScript>() != null)
        {
            damager.GetComponent<BountyScript>().totalGold += GetComponent<BountyScript>().baseGoldBounty;
        }
        if(GetComponent<Barracks>()!=null)
        {
            damager.GetComponent<BountyScript>().totalGold += Random.Range(GetComponent<BountyScript>().towerBounty.x, GetComponent<BountyScript>().towerBounty.y);
            if (GetComponent<Barracks>().paralBarracks != null)
            {
                GetComponent<Barracks>().paralBarracks.barrackLevel++;
            }
            //Destroy(this.gameObject);
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }
        else if(GetComponent<TowerScript>()!=null)
        {
            damager.GetComponent<BountyScript>().totalGold += Random.Range(GetComponent<BountyScript>().towerBounty.x, GetComponent<BountyScript>().towerBounty.y);
            if (GetComponent<TowerScript>().linkedTowers.Count > 0)
            {
                for (int i = 0; i < GetComponent<TowerScript>().linkedTowers.Count; i++)
                {
                    GetComponent<TowerScript>().linkedTowers[i].gameObject.GetComponent<HealthScrpStruc>().isInvulnerable = false;
                    //GetComponent<TowerScript>().linkedTowers[i].GetComponent<HealthScrpStruc>().isInvulnerable = false;
                }
            }
            Destroy(this.gameObject);
        }
        else if(GetComponent<AncientScript>()!=null)
        {
            Debug.Log("Faction : " + GameManager.instance);
            GameState.Instance.AssignWinner(this.gameObject.GetComponent<Factions>().unitFaction);
            Destroy(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    public void ShowFloatingText(float damage)
    {
        var go = Instantiate(floatingText, new Vector3(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z), Quaternion.identity, transform);
        go.GetComponent<TextMesh>().text = damage.ToString();
    }

    public void DestroyBarrack()
    {
        if (GetComponent<Barracks>().paralBarracks != null)
        {
            GetComponent<Barracks>().paralBarracks.barrackLevel++;
        }
    }

    public void DestroyTower()
    {
        if (GetComponent<TowerScript>().linkedTowers.Count > 0)
        {
            for (int i = 0; i < GetComponent<TowerScript>().linkedTowers.Count; i++)
            {
                GetComponent<TowerScript>().linkedTowers[i].gameObject.GetComponent<HealthScrpStruc>().isInvulnerable = false;
            }
        }
    }
}
