﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProjectile : MonoBehaviour
{
    public GameObject origin;
    public Transform target;
    public float speed;
    public float damage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(this.transform.position, target.transform.position, speed * Time.deltaTime);
        if(target.gameObject==null)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == target.gameObject.name)
        {
            if (other.gameObject.GetComponent<HealthScript>() != null)
            {
                HealthScript hp = other.gameObject.GetComponent<HealthScript>();
                hp.TakeDamage(damage, origin);
            }
            else if (other.gameObject.GetComponent<HealthScrpStruc>() != null)
            {
                HealthScrpStruc hp = other.gameObject.GetComponent<HealthScrpStruc>();
                hp.TakeDamage(damage, origin);
            }
            Destroy(this.gameObject);
        }
    }
}
