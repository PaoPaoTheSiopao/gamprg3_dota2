﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUnit : MonoBehaviour
{
    public float cameraSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, 80, 490), this.transform.position.y, Mathf.Clamp(this.transform.position.z, 35, 472));
        if(Input.mousePosition.x<=0)
        {
            transform.Translate(Vector3.left * cameraSpeed * Time.deltaTime);
        }
        else if(Input.mousePosition.x>=Screen.width-5)
        {
            transform.Translate(-Vector3.left * cameraSpeed * Time.deltaTime);
        }
        if(Input.mousePosition.y<=0)
        {
            transform.Translate(-Vector3.forward * cameraSpeed * Time.deltaTime);
        }
        else if(Input.mousePosition.y>=Screen.height-5)
        {
            transform.Translate(Vector3.forward * cameraSpeed * Time.deltaTime);
        }
    }
}
